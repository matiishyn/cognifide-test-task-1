jQuery(function ($) {
	var App = {
        dataPath: '../data/',
		init: function() {
            // initialization of maps
            this.initMap();
            // paste comments for main image
			this.renderTemplates('#image-comment-template', 'image-comments.json', '.image-comments');
			// paste posts to main section
            this.renderTemplates('#small-posts-tmpl', 'small-posts.json', '.small-posts');
		},
		renderTemplates: function(tmpl, data, placeholder) {
			var that = this,
				source   = $(tmpl).html(),
				template = Handlebars.compile(source);

			$.getJSON(this.dataPath + data, function(resp) {
				$(placeholder).html(that.parseData(resp.data, template));
			});
		},
		parseData: function(array, template) {
			var html = '';
			array.forEach(function(item) {
				html += template(item);
			});
			return html;
		},
        initMap: function() {
            var mapCanvas = document.getElementById('map-canvas');
            var mapOptions = {
                center: new google.maps.LatLng(52.2167, 21.0333),
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(mapCanvas, mapOptions)
        }
	};
	App.init();
});